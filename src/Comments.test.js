import React from 'react'
import Comments from './Comments'
import Comment from './Comment'
import { shallow } from 'enzyme'

it('Teste 1', () => {
    const colComments = {
        a: { id: '1', comment: 'meu teste um' },
        b: { id: '2', comment: 'meu teste dois' },
        c: { id: '3', comment: 'meu teste tres' }
    }

    const wrapper = shallow(<Comments comments={colComments} />)
    expect(wrapper.find(Comment).length).toBe(3)
})


it('Teste 2', () => {
    const colComments = {
        y: { id: '1', comment: 'teste um' },
        z: { id: '3', comment: 'teste tres' }
    }

    const wrapper = shallow(<Comments comments={colComments} />)
    expect(wrapper.find(Comment).get(1).props.obj.comment).toBe('teste tres')
    expect(wrapper.find(Comment).get(0).props.obj).toBe(colComments.y)
})


it('Teste 3', () => {
    const colComments = {}
    const wrapper = shallow(<Comments comments={colComments} />)
    expect(wrapper.find(Comment).length).toBe(0)
    expect(wrapper.html()).toBe('<ul></ul>')
})
