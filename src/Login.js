import React, { Component } from 'react'

class Login extends Component {

    state = {
        email: '',
        password: ''
    }

    handleChange = (e) => (
        this.setState({
            [e.target.name]: e.target.value
        })
    )

    validar = (e) => {
        e.preventDefault();

        if (this.state.email !== '' && this.state.password !== '') {
            this.props.logar(this.state.email, this.state.password)
        } else {
            alert("Preencha os campos corretamente")
        }
    }

    render() {

        const msgError = {
            "auth/user-not-found": "Email não existente",
            "auth/invalid-email": "Email invalido",
            "auth/wrong-password": "Senha invalida"
        }

        return (
            <div>
                <h4>Login</h4>

                <form className='form-inline'>
                    <input className='form-control mr-1' type='email' value={this.state.email} name='email' onChange={this.handleChange}></input>
                    <input className='form-control mr-1' type='password' value={this.state.password} name='password' onChange={this.handleChange}></input>
                    <button className='btn btn-success' onClick={this.validar}>Validar</button>
                </form>

                {this.props.isAuthError &&
                    <p className='alert alert-danger'>
                        <b>Erro encontrado:</b> {msgError[this.props.authError]}
                    </p>
                }
            </div>
        );
    }
}

export default Login;