import React, { Component } from 'react';

class NewComment extends Component {

    state = {
        newComment: ''
    }

    handleChange = (e) => (
        this.setState({
            newComment: e.target.value
        })
    )

    sendComment = () => {
        this.props.sendComment(this.state.newComment)
        
        this.setState({
            newComment: ''
        })
    }

    render() {
        return (
            <div>
                <h3>Novo comentario</h3>
                <textarea value={this.state.newComment} onChange={this.handleChange} rows="5"></textarea>
                <button onClick={this.sendComment}>Salvar</button>
            </div>
        )
    }
}

export default NewComment