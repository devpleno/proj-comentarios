import React from 'react';

const Comment = ({ obj }) => {
    const meuComment = (obj) ? obj.comment : 'vazio'

    return (
        <div className='card mt-2'>
            <div className='card-body'>
                {meuComment}
            </div>
            <div className='card-footer'>
                Enviado por: {obj.email ? obj.email : 'Anônimo'}
            </div>
        </div>
    )
}

export default Comment;