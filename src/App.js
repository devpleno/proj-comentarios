import React, { Component } from 'react';
// import uuid from 'uuid'

import Login from './Login'
import Signup from './Signup'
import Comments from './Comments'
import NewComment from './NewComment'
import Usuario from './Usuario'

import 'bootstrap-css-only'

class App extends Component {

  state = {
    isLoading: false,
    isAuth: false,
    usuario: '',
    isAuthError: false,
    authError: '',
    comments: {},
    isSignupError: false,
    signupError: '',
    userScreen: 'login'
  }

  componentDidMount() {
    this.setState({ isLoading: true })
    const comments = this.props.database.ref('comments')

    comments.on('value', snapshot => {
      this.setState({ comments: snapshot.val() })
      this.setState({ isLoading: false })
    })

    this.props.auth.onAuthStateChanged(user => {

      if (user) {

        this.setState({
          isAuth: true,
          usuario: user
        })

      } else {

        this.setState({
          isAuth: false,
          usuario: ''
        })

      }

    })
  }

  sendComment = comment => {

    const id = this.props.database.ref().child('comments').push().key;
    // const id = uuid();

    const comments = {}
    comments['comments/' + id] = { comment, email: this.state.usuario.email, id: this.state.usuario.uid }
    this.props.database.ref().update(comments)
  }

  limparCampos = () => {
    this.setState({
      isAuthError: false,
      authError: ''
    })

    this.setState({
      isSignupError: false,
      signupError: ''
    })
  }

  logar = async (email, password) => {

    this.limparCampos();    

    try {
      await this.props.auth.signInWithEmailAndPassword(email, password)

    } catch (e) {

      this.setState({
        isAuthError: true,
        authError: e.code
      })

    }
  }

  novo = async (email, password) => {

    this.limparCampos();

    try {
      await this.props.auth.createUserWithEmailAndPassword(email, password)
      this.changeScreen()

    } catch (e) {

      this.setState({
        isSignupError: true,
        signupError: e.code
      })

    }

  }

  logout = () => {
    this.props.auth.signOut()
  }

  changeScreen = () => {
    this.limparCampos();   

    this.setState({
      userScreen: (this.state.userScreen === 'login') ? 'signup' : 'login'
    })
  }

  render() {
    return (
      <div className='container mt-3'>

        {!this.state.isAuth && this.state.userScreen === 'login' &&
          <Login logar={this.logar} isAuthError={this.state.isAuthError} authError={this.state.authError} />
        }

        {!this.state.isAuth && this.state.userScreen === 'signup' &&
          <Signup novo={this.novo} isSignupError={this.state.isSignupError} signupError={this.state.signupError} />
        }

        {!this.state.isAuth &&
          <button className='btn btn-info mt-2' onClick={this.changeScreen}>{(this.state.userScreen === 'login') ? "Cadastrar-se" : "Já tenho uma conta!"}</button>
        }

        {this.state.isAuth &&
          <div>
            <Usuario usuario={this.state.usuario} logout={this.logout} />
            <NewComment sendComment={this.sendComment} />
          </div>
        }

        {this.state.isLoading &&
          <p>
            Carregando...
          </p>
        }

        {!this.state.isLoading &&
          <div>
            <h3>Comentarios</h3>
            <Comments comments={this.state.comments} />
          </div>
        }

      </div>
    );
  }
}

export default App;
