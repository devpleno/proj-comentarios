import firebase from 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyDjq12bmnL5IRyuXTr1xirKh2fR7r5B8jc",
    authDomain: "proj-comments.firebaseapp.com",
    databaseURL: "https://proj-comments.firebaseio.com",
    projectId: "proj-comments",
    storageBucket: "",
    messagingSenderId: "380201406448"
};

firebase.initializeApp(config);

export const database = firebase.database()
export const auth = firebase.auth()