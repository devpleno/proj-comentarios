import React from 'react'
import App from './App'
import NewComment from './NewComment'
import Comments from './Comments'

import { shallow } from 'enzyme'
import { EventEmitter } from 'events'

it('Teste 1', () => {
    const newDatabase = {
        ref: jest.fn()
    }

    newDatabase.ref.mockReturnValue({
        on: jest.fn()
    })

    const wrapper = shallow(<App database={newDatabase} />)

    expect(wrapper.find(NewComment).length).toBe(1)
    expect(wrapper.find('p').length).toBe(1)
})


it('Teste 2', () => {

    const newDatabase = {
        ref: jest.fn()
    }

    const newChild = jest.fn()
    const newUpdate = jest.fn()

    newDatabase.ref.mockReturnValue({
        on: jest.fn(),
        child: newChild,
        update: newUpdate
    })

    const newPush = jest.fn()

    newChild.mockReturnValue({
        push: newPush
    })

    newPush.mockReturnValue({
        key: '1'
    })

    const wrapper = shallow(<App database={newDatabase} />)

    wrapper.instance().sendComment('new comment high')

    expect(newChild).toBeCalledWith('comments')

    expect(newUpdate).toBeCalledWith({ 'comments/1': { 'comment': 'new comment high' } })
})


it('Teste 3', () => {
    const newDatabase = {
        ref: jest.fn()
    }

    const newEvent = new EventEmitter()
    newDatabase.ref.mockReturnValue(newEvent)

    const wrapper = shallow(<App database={newDatabase} />)

    expect(wrapper.find('p').length).toBe(1)

    const colComments = {
        a: { 'comment': 'comment 1' },
        b: { 'comment': 'comment 2' }
    }

    const newVal = jest.fn()
    newVal.mockReturnValue(colComments)

    expect(wrapper.state().isLoading).toBeTruthy()
    // console.log(wrapper.state())

    newEvent.emit('value', {
        val: newVal
    })

    wrapper.update()

    expect(wrapper.state().isLoading).toBeFalsy()
    expect(wrapper.state().comments).toBe(colComments)
    expect(wrapper.find(Comments).get(0).props.comments).toBe(colComments)
    expect(wrapper.find(NewComment).get(0).props.sendComment).toBe(wrapper.instance().sendComment)
    expect(wrapper.find('p').length).toBe(0)
})
