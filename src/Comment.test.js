import React from 'react'
import Comment from './Comment'
import { render } from 'enzyme'

it('Teste 1', () => {
    const newComment = {
        comment: 'meu teste'
    }

    const wrapper = render(<Comment obj={newComment} />)
    expect(wrapper.text()).toBe('Comentário: meu teste')
})

it('Teste 2', () => {
    const newComment = {
        comment: 'minha casa minha vida'
    }

    const wrapper = render(<Comment obj={newComment} />)
    expect(wrapper.text()).toBe('Comentário: minha casa minha vida')
})

it('Teste 3', () => {
    const wrapper = render(<Comment />)
    expect(wrapper.text()).toBe('Comentário: vazio')
})