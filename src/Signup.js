import React, { Component } from 'react'

class Signup extends Component {

    state = {
        email: '',
        password: ''
    }

    handleChange = (e) => (
        this.setState({
            [e.target.name]: e.target.value
        })
    )

    novo = (e) => {
        e.preventDefault()

        if (this.state.email !== '' && this.state.password !== '') {
            this.props.novo(this.state.email, this.state.password)
        } else {
            alert("Preencha os campos corretamente")
        }
    }

    render() {

        const msgError = {
            "auth/email-already-in-use": "Email já existente",
            "auth/weak-password": "A senha é fraca",
            "auth/invalid-email": "Email invalido"
        }

        return (
            <div>
                <h4>Cadastre-se</h4>

                <form className='form-inline'>
                    <input className='form-control mr-1' type='email' value={this.state.email} name='email' onChange={this.handleChange}></input>
                    <input className='form-control mr-1' type='password' value={this.state.password} name='password' onChange={this.handleChange}></input>
                    <button className='btn btn-success' onClick={this.novo}>Salvar</button>
                </form>

                {this.props.isSignupError &&
                    <p className='alert alert-danger'>
                        <b>Erro encontrado:</b> {msgError[this.props.signupError]}
                    </p>
                }
            </div>
        );
    }
}

export default Signup;