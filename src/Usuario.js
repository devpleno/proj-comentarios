import React, { Component } from 'react';

class Usuario extends Component {

    logout = () => (
        this.props.logout()
    )

    render() {
        return (
            <div>
                Bem vindo, {this.props.usuario.email} <br />
                <button onClick={this.logout}>Sair do sistema</button>
            </div>
        )
    }
}

export default Usuario