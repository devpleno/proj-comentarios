import React from 'react'
import NewComment from './NewComment'
import { shallow } from 'enzyme'

it('Teste 1', () => {
    const wrapper = shallow(<NewComment />)

    const novoEvento = { target: { value: 'meu texto' } }
    wrapper.find('textarea').simulate('change', novoEvento)

    expect(wrapper.state().newComment).toBe('meu texto')
})


it('Teste 2', () => {
    const sendCommentMock = jest.fn()
    const wrapper = shallow(<NewComment sendComment={sendCommentMock} />)

    const novoEvento = { target: { value: 'meu segundo texto' } }
    wrapper.find('textarea').simulate('change', novoEvento)
    
    wrapper.find('button').simulate('click')

    expect(sendCommentMock.mock.calls[0][0]).toBe('meu segundo texto')
    expect(sendCommentMock).toBeCalledWith('meu segundo texto')

    expect(wrapper.state().newComment).toBe('')
})
