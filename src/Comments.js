import React from 'react';
import Comment from './Comment';

const Comments = ({ comments }) => {
    const keys = Object.keys(comments)

    return (
        <div>
            {
                keys.map((obj, i) => (
                    <Comment obj={comments[obj]} key={i} />
                ))
            }
        </div>
    )
}

export default Comments